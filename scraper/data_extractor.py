import pandas as pd
import datetime as dt
import time

start_time = time.time()
# I'll find nearest date from schedule and add to csv
# https://stackoverflow.com/questions/32237862/find-the-closest-date-to-a-given-date
def nearest(date_list, date):
    nearest = min(date_list, key=lambda x: abs(dt.datetime.strptime(x, "%Y-%m-%d").date() - dt.datetime.strptime(date, "%Y-%m-%d").date()))
    return nearest

orig_df = pd.read_csv('companies_fundamentals_data.csv')
stocks_df = pd.read_csv('../data/stock-quotes.csv', names=["Date", "Adjusted_Price", "Ticker"])

c = 0

target_csv = 'stock-data-extracted.csv'

c = 0

target=[]

temp_df = pd.DataFrame()

for i, r in orig_df.iterrows():
    if c <= 21111:
        c += 1
        print(c)
        continue
    else:
        reportdate = r['reportperiod']
        ticker = r['ticker']

        reportdate_as_dt = dt.datetime.strptime(reportdate, "%Y-%m-%d")

        try:
            last_year_report_date = dt.datetime(reportdate_as_dt.year - 1, reportdate_as_dt.month,
                                                reportdate_as_dt.day).strftime("%Y-%m-%d")
            comparable_dates = stocks_df[stocks_df['Ticker'] == ticker].Date.to_list()
            ly_near_date = nearest(comparable_dates, last_year_report_date)

            ty_near_date = nearest(comparable_dates, reportdate)
            ly_price = \
            stocks_df[(stocks_df['Ticker'] == ticker) & (stocks_df['Date'] == ly_near_date)].Adjusted_Price.to_list()[0]
            ty_price = \
            stocks_df[(stocks_df['Ticker'] == ticker) & (stocks_df['Date'] == ty_near_date)].Adjusted_Price.to_list()[0]

            target.append([ticker, reportdate, ly_near_date, ly_price, ty_near_date, ty_price])
            temp_df = pd.DataFrame([target[len(target) - 1]],
                                   columns=['ticker', 'reportperiod', 'ly_date', 'ly_price', 'ty_date', 'ty_price'])
            with open(target_csv, 'a') as f:
                temp_df.to_csv(f, header=False)
            # Print
            c += 1
            if c % 10 == 0 and c > 0:
                print(c, str(time.time() - start_time))
        except KeyError:
            continue
        except TypeError:
            continue
        except ValueError:
            target.append([ticker, reportdate, None, None, None, None])
            temp_df = pd.DataFrame([target[len(target) - 1]],
                                   columns=['ticker', 'reportperiod', 'ly_date', 'ly_price', 'ty_date', 'ty_price'])
            with open(target_csv, 'a') as f:
                temp_df.to_csv(f, header=False)
            continue


