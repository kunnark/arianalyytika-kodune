import pandas as pd
from pandas_datareader import data
from pandas_datareader import _utils
import time
import asyncio
import concurrent.futures
import datetime
import requests_cache
import os

os.chdir(os.getcwd())
start_time = time.time()
df = pd.read_csv('companies_fundamentals_data.csv')
df.head()

expire_after = datetime.timedelta(days=10)
session = requests_cache.CachedSession(cache_name='cache', backend='sqlite')

tickers_count = len(list(df['ticker'].unique()))

def data_store(start, end):
    tickers = list(df['ticker'].unique())[start:end]
    return tickers

async def resolve(df):
    return df

async def get_data_async(ticker, start_date, end_date):
    try:
        panel_data = await resolve(data.DataReader(ticker, 'yahoo', start_date, end_date, session=session))
        return panel_data
    except KeyError:
        pass
    # If no data fetched:
    except _utils.RemoteDataError:
        pass

async def fetch(start, end):
    i = 0
    tickers = data_store(start, end)
    for t in tickers:
        # Fetch data dates:
        start_date = pd.Timestamp(min(df[df['ticker'] == t].reportperiod))
        start_date = str(pd.Timestamp(start_date.year - 1, start_date.month, start_date.day))
        end_date = max(df[df['ticker'] == t].reportperiod)
        fetched_df = await get_data_async(t, start_date, end_date)

        if fetched_df is not None and t is not None:
            fetched_df['ticker'] = t
            fetched_df = fetched_df.drop(columns=['High','Low','Open','Close','Volume'])
            fetched_df.append(fetched_df)
            fetched_df.to_csv('stock-quotes.csv', mode='a', header=False)
        i += 1
        if i > 0 and i % 100 == 0:
            print("{} {}".format(i, str(time.time() - start_time)))


with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
    executor.submit(asyncio.run(fetch(0, 20)))

print("Done: {}".format(str(time.time() - start_time)))
