import pandas as pd
import time
df = pd.read_csv('../data/cleaned-data.csv')

start_time = time.time()
# Columns to add new values:
column_headers = [
  'assets',
  'assetsavg',
  'assetsc',
  'assetturnover',
  'bvps',
  'capex',
  'cashneq',
  'cashnequsd',
  'cor',
  'consolinc',
  'currentratio',
  'de',
  'debt',
  'debtc',
  'debtnc',
  'debtusd',
  'deferredrev',
  'depamor',
  'deposits',
  'divyield',
  'dps',
  'ebit',
  'ebitda',
  'ebitdamargin',
  'ebitdausd',
  'ebt',
  'eps',
  'epsdil',
  'epsusd',
  'equity',
  'equityavg',
  'equityusd',
  'ev',
  'evebit',
  'evebitda',
  'fcf',
  'fcfps',
  'fxusd',
  'gp',
  'grossmargin',
  'intangibles',
  'intexp',
  'invcap',
  'invcapavg',
  'inventory',
  'investments',
  'investmentsc',
  'investmentsnc',
  'liabilities',
  'liabilitiesnc',
  'marketcap',
  'ncf',
  'ncfbus',
  'ncfinv',
  'ncfo',
  'ncfx',
  'netinc',
  'netinccmn',
  'netinccmnusd',
  'netincdis',
  'netincnci',
  'netmargin',
  'opex',
  'opinc',
  'payables',
  'payoutratio',
  'pb',
  'pe',
  'pe1',
  'ppnenet',
  'prefdivis',
  'price',
  'ps',
  'ps1',
  'receivables',
  'retearn',
  'revenue',
  'revenueusd',
  'rnd',
  'roa',
  'roe',
  'roic',
  'ros',
  'sbcomp',
  'sgna',
  'sharefactor',
  'sharesbas',
  'shareswa',
  'shareswadil',
  'sps',
  'tangibles',
  'taxassets',
  'taxexp',
  'taxliabilities',
  'tbvps',
  'workingcapital'
]
# Create additional columns on data with last-period change
rows = df.shape[0]
for i in range(0, len(column_headers)):
    column_header = column_headers[i]
    add_header = column_header + "_change"
    target = []
    for index, row in df.iterrows():
        ticker = row['ticker']
        current_reportperiod = row['reportperiod']
        last_reportperiod = df[(df.index == index + 1) & (df.ticker == ticker)].reportperiod.to_list()
        if len(last_reportperiod) > 0:
          try:
            value = df[df.reportperiod == current_reportperiod][column_header].to_list()[0]/df[df.reportperiod == last_reportperiod[0]][column_header].to_list()[0] *100.0
            target.append(value)
          except ZeroDivisionError:
            target.append(None)
            continue
        else:
            target.append(None)
    df[add_header] = target
    df.to_csv('cleaned-data-1-change-added.csv')
    current_time = time.time() - start_time
    print("{} {} Estimated {}".format(i, str(current_time), str((current_time / (i + 1)) * (len(column_headers) + 1))))