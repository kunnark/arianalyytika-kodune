library(caret)
library(tidyverse)
library(dplyr)
library(forecast)
library(e1071) #skewness

# Author: Kunnar Kukk
# Data Analytics Course Project

setwd("~/Desktop/andmeanalyytika/projekt")
stocks <- read_delim("stocks-change-added.csv", ",", escape_double = FALSE, trim_ws = TRUE)

# Remove non-numeric columns from model, otherwise glm won't calculate:
# Idea from http://datamining.togaware.com/survivor/Remove_Non_Numeric.html
stocks <- stocks[,sapply(stocks, is.numeric)]

# Asendame NA väärtused 0-ga
stocks[is.na(stocks)] = 0
stocks <- stocks[is.finite(rowSums(stocks)),]
stocks <- na.omit(stocks)
################################################################################
# Simple statisticks
################################################################################
mean_stock_price_change <- stocks %>%
  summarise(mean_change=mean(stock_price_change))

mean <- mean_stock_price_change

# Standard deviation
std <- sd(stocks$stock_price_change)

# Median
median(stocks$stock_price_change, na.rm = FALSE)

# Skewness
skewness(stocks$stock_price_change)

# Kurtosis
kurtosis(stocks$stock_price_change)

max(stocks$stock_price_change)
min(stocks$stock_price_change)

qplot(stocks$stock_price_change, geom="histogram") 

################################################################################
# Kui hinnakasv oli > 100%, siis aktsia hind kasvas, vastasel juhul vähenes.
stocks <- stocks %>%
  mutate(stock_price_grows = ifelse(stock_price_change > 100, 1, 0))

# stocks[!complete.cases(stocks), ]

# Create ID column for data
stocks <- tibble::rowid_to_column(stocks, "Id2")

# Create training and validation partition
set.seed(1)
training_set <- stocks %>%
  sample_frac(0.6)

validation_set <- anti_join(stocks, training_set, by='Id2')

################################################################################
# Using logistic regression determine variable impact
################################################################################
model <- glm(
  stock_price_grows ~
    + debt_change
  + assetsavg_change
  + assetsc_change
  + assetturnover_change
  + bvps_change
  + capex_change
  + cashneq_change
  + cashnequsd_change
  + cor_change
  + consolinc_change
  + currentratio_change
  + de_change
  + debt_change
  + debtc_change
  + debtnc_change
  + debtusd_change
  + deferredrev_change
  + depamor_change
  + deposits_change
  + divyield_change
  + dps_change
  + ebit_change
  + ebitda_change
  + ebitdamargin_change
  + ebitdausd_change
  + ebt_change
  + eps_change
  + epsdil_change
  + epsusd_change
  + equity_change
  + equityavg_change
  + equityusd_change
  + ev_change
  + evebit_change
  + evebitda_change
  + fcf_change
  + fcfps_change
  + fxusd_change
  + gp_change
  + grossmargin_change
  + intangibles_change
  + intexp_change
  + invcap_change
  + invcapavg_change
  + inventory_change
  + investments_change
  + investmentsc_change
  + investmentsnc_change
  + liabilities_change
  + liabilitiesnc_change
  + marketcap_change
  + ncf_change
  + ncfbus_change
  + ncfinv_change
  #
  + ncfo_change
  + ncfx_change
  + netinc_change
  + netinccmn_change
  + netinccmnusd_change
  + netincdis_change
  + netincnci_change
  + netmargin_change
  + opex_change
  + opinc_change
  + payables_change
  #
  + payoutratio_change
  + pb_change
  + pe_change
  + pe1_change
  + ppnenet_change
  + prefdivis_change
  + price_change
  + ps_change
  + ps1_change
  + receivables_change
  + retearn_change
  + revenue_change
  + revenueusd_change
  + rnd_change
  + roa_change
  + roe_change
  + roic_change
  + ros_change
  + sbcomp_change
  + sgna_change
  + sharefactor_change
  + sharesbas_change
  + shareswa_change
  # With following 3 parameters model doesn't convergate:
  # + shareswadil_change
  # + sps_change
  + tangibles_change
  + taxassets_change
  + taxexp_change
  + taxliabilities_change
  # + tbvps_change
  + workingcapital_change
  ,
  data = training_set, family = binomial)

# About NA removal https://stats.stackexchange.com/questions/46692/how-the-na-values-are-treated-in-glm-in-r

options(scipen=999)
summary(model)


# Use forward selection
# Specify lower bound model (with no predictors) and upper bound model (will all predictors) and use function step()

# Backward elimination
no_predictor_model <- glm(stock_price_grows ~ 1, data = training_set)

all_predictor_model <- glm(stock_price_grows  ~ 
                          + debt_change
                          + assetsavg_change
                          + assetsc_change
                          + assetturnover_change
                          + bvps_change
                          + capex_change
                          + cashneq_change
                          + cashnequsd_change
                          + cor_change
                          + consolinc_change
                          + currentratio_change
                          + de_change
                          + debt_change
                          + debtc_change
                          + debtnc_change
                          + debtusd_change
                          + deferredrev_change
                          + depamor_change
                          + deposits_change
                          + divyield_change
                          + dps_change
                          + ebit_change
                          + ebitda_change
                          + ebitdamargin_change
                          + ebitdausd_change
                          + ebt_change
                          + eps_change
                          + epsdil_change
                          + epsusd_change
                          + equity_change
                          + equityavg_change
                          + equityusd_change
                          + ev_change
                          + evebit_change
                          + evebitda_change
                          + fcf_change
                          + fcfps_change
                          + fxusd_change
                          + gp_change
                          + grossmargin_change
                          + intangibles_change
                          + intexp_change
                          + invcap_change
                          + invcapavg_change
                          + inventory_change
                          + investments_change
                          + investmentsc_change
                          + investmentsnc_change
                          + liabilities_change
                          + liabilitiesnc_change
                          + marketcap_change
                          + ncf_change
                          + ncfbus_change
                          + ncfinv_change
                          + ncfo_change
                          + ncfx_change
                          + netinc_change
                          + netinccmn_change
                          + netinccmnusd_change
                          + netincdis_change
                          + netincnci_change
                          + netmargin_change
                          + opex_change
                          + opinc_change
                          + payables_change
                          + payoutratio_change
                          + pb_change
                          + pe_change
                          + pe1_change
                          + ppnenet_change
                          + prefdivis_change
                          + price_change
                          + ps_change
                          + ps1_change
                          + receivables_change
                          + retearn_change
                          + revenue_change
                          + revenueusd_change
                          + rnd_change
                          + roa_change
                          + roe_change
                          + roic_change
                          + ros_change
                          + sbcomp_change
                          + sgna_change
                          + sharefactor_change
                          + sharesbas_change
                          + shareswa_change
                          # + shareswadil_change
                          # + sps_change
                          + tangibles_change
                          + taxassets_change
                          + taxexp_change
                          + taxliabilities_change
                          # + tbvps_change
                          + workingcapital_change,
                          data = training_set)
# Backward elimination
backward_selection_results <- step(all_predictor_model, direction = "backward")
summary(backward_selection_results)
tab_model(backward_selection_results)

# Prediction
stock_prediction <- predict(all_predictor_model, validation_set)
summary(stock_prediction)
accuracy <- accuracy(stock_prediction, validation_set$stock_price_grows)
accuracy

###########################################################################
# Otsustuspuud
###########################################################################
library(rpart)
library(rpart.plot)
library(sjPlot)
library(tidyverse)
library(randomForest)
library(adabag)


classification_tree <- rpart(stock_price_grows ~ 
                              + assetsavg_change
                              + assetturnover_change
                              + deposits_change
                              + divyield_change
                              + dps_change
                              + ebit_change
                              + ev_change
                              + grossmargin_change
                              + inventory_change
                              + liabilities_change
                              + ncfbus_change
                              + netincnci_change
                              + netmargin_change 
                              + payables_change
                              + pe1_change
                              + prefdivis_change 
                              + ps_change
                              + receivables_change
                              + rnd_change
                              + sgna_change
                              + sharefactor_change
                              + tangibles_change
                              + taxassets_change
                               , data = validation_set, 
                             control = rpart.control(maxdepth = 5),
                             method = "class")
prediction <- predict(classification_tree, training_set, type = "class")

summary(classification_tree)

# Plot classification tree
prp(classification_tree, type = 2, extra = 2, under = TRUE, split.font = 2, varlen = -10)

#### Random forest ####
random_forest <- randomForest(as.factor(stock_price_grows) ~ 
                              + assetsavg_change
                              + assetturnover_change
                              + deposits_change
                              + divyield_change
                              + dps_change
                              + ebit_change
                              + ev_change
                              + grossmargin_change
                              + inventory_change
                              + liabilities_change
                              + ncfbus_change
                              + netincnci_change
                              + netmargin_change 
                              + payables_change
                              + pe1_change
                              + prefdivis_change 
                              + ps_change
                              + receivables_change
                              + rnd_change
                              + sgna_change
                              + sharefactor_change
                              + tangibles_change
                              + taxassets_change
                              , data = validation_set, ntree = 500,
                              mtry = 4, nodesize = 5, importance = TRUE)

## variable importance plot
varImpPlot(random_forest, type = 1)
## confusion matrix
random_forest_prediction <- predict(random_forest, validation_set)
confusionMatrix(random_forest_prediction, as.factor(validation_set$stock_price_grows))

###########################################################################
# Korrelatsioonimaatriks
###########################################################################
correlations_change <- data.frame(cor(select(stocks,
                                             stock_price_change,
                                             assetsavg_change,
                                             assetturnover_change,
                                             deposits_change,
                                             divyield_change,
                                             dps_change,
                                             ebit_change,
                                             ev_change,
                                             grossmargin_change,
                                             inventory_change,
                                             liabilities_change,
                                             marketcap_change,
                                             ncfbus_change,
                                             netincnci_change,
                                             netmargin_change,
                                             payables_change,
                                             pe1_change,
                                             prefdivis_change,
                                             price_change,
                                             ps_change,
                                             receivables_change,
                                             rnd_change,
                                             sgna_change,
                                             sharefactor_change,
                                             tangibles_change,
                                             taxassets_change
 
)))

################################################################################
# Plots
################################################################################

# Visualiseerime korrelatsiooni
install.packages("ggcorrplot")
library(ggcorrplot)
ggcorrplot(correlations_change, method = "square")

ggplot(stocks, aes(x=stock_price_change)) + geom_histogram(binwidth=2)
