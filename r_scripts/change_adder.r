library(caret)
library(tidyverse)
library(dplyr)


setwd("~/Desktop/andmeanalyytika/projekt")
stocks <- read_delim("final-companies-results-data.csv", ",", escape_double = FALSE, trim_ws = TRUE)

# Change parameters generator
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(debt_change = (debt/lead(debt))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(assetsavg_change = (assetsavg/lead(assetsavg))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(assetsc_change = (assetsc/lead(assetsc))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(assetturnover_change = (assetturnover/lead(assetturnover))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(bvps_change = (bvps/lead(bvps))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(capex_change = (capex/lead(capex))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(cashneq_change = (cashneq/lead(cashneq))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(cashnequsd_change = (cashnequsd/lead(cashnequsd))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(cor_change = (cor/lead(cor))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(consolinc_change = (consolinc/lead(consolinc))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(currentratio_change = (currentratio/lead(currentratio))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(de_change = (de/lead(de))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(debt_change = (debt/lead(debt))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(debtc_change = (debtc/lead(debtc))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(debtnc_change = (debtnc/lead(debtnc))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(debtusd_change = (debtusd/lead(debtusd))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(deferredrev_change = (deferredrev/lead(deferredrev))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(depamor_change = (depamor/lead(depamor))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(deposits_change = (deposits/lead(deposits))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(divyield_change = (divyield/lead(divyield))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(dps_change = (dps/lead(dps))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(ebit_change = (ebit/lead(ebit))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(ebitda_change = (ebitda/lead(ebitda))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(ebitdamargin_change = (ebitdamargin/lead(ebitdamargin))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(ebitdausd_change = (ebitdausd/lead(ebitdausd))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(ebt_change = (ebt/lead(ebt))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(eps_change = (eps/lead(eps))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(epsdil_change = (epsdil/lead(epsdil))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(epsusd_change = (epsusd/lead(epsusd))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(equity_change = (equity/lead(equity))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(equityavg_change = (equityavg/lead(equityavg))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(equityusd_change = (equityusd/lead(equityusd))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(ev_change = (ev/lead(ev))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(evebit_change = (evebit/lead(evebit))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(evebitda_change = (evebitda/lead(evebitda))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(fcf_change = (fcf/lead(fcf))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(fcfps_change = (fcfps/lead(fcfps))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(fxusd_change = (fxusd/lead(fxusd))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(gp_change = (gp/lead(gp))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(grossmargin_change = (grossmargin/lead(grossmargin))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(intangibles_change = (intangibles/lead(intangibles))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(intexp_change = (intexp/lead(intexp))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(invcap_change = (invcap/lead(invcap))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(invcapavg_change = (invcapavg/lead(invcapavg))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(inventory_change = (inventory/lead(inventory))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(investments_change = (investments/lead(investments))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(investmentsc_change = (investmentsc/lead(investmentsc))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(investmentsnc_change = (investmentsnc/lead(investmentsnc))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(liabilities_change = (liabilities/lead(liabilities))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(liabilitiesnc_change = (liabilitiesnc/lead(liabilitiesnc))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(marketcap_change = (marketcap/lead(marketcap))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(ncf_change = (ncf/lead(ncf))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(ncfbus_change = (ncfbus/lead(ncfbus))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(ncfinv_change = (ncfinv/lead(ncfinv))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(ncfo_change = (ncfo/lead(ncfo))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(ncfx_change = (ncfx/lead(ncfx))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(netinc_change = (netinc/lead(netinc))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(netinccmn_change = (netinccmn/lead(netinccmn))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(netinccmnusd_change = (netinccmnusd/lead(netinccmnusd))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(netincdis_change = (netincdis/lead(netincdis))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(netincnci_change = (netincnci/lead(netincnci))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(netmargin_change = (netmargin/lead(netmargin))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(opex_change = (opex/lead(opex))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(opinc_change = (opinc/lead(opinc))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(payables_change = (payables/lead(payables))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(payoutratio_change = (payoutratio/lead(payoutratio))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(pb_change = (pb/lead(pb))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(pe_change = (pe/lead(pe))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(pe1_change = (pe1/lead(pe1))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(ppnenet_change = (ppnenet/lead(ppnenet))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(prefdivis_change = (prefdivis/lead(prefdivis))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(price_change = (price/lead(price))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(ps_change = (ps/lead(ps))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(ps1_change = (ps1/lead(ps1))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(receivables_change = (receivables/lead(receivables))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(retearn_change = (retearn/lead(retearn))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(revenue_change = (revenue/lead(revenue))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(revenueusd_change = (revenueusd/lead(revenueusd))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(rnd_change = (rnd/lead(rnd))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(roa_change = (roa/lead(roa))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(roe_change = (roe/lead(roe))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(roic_change = (roic/lead(roic))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(ros_change = (ros/lead(ros))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(sbcomp_change = (sbcomp/lead(sbcomp))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(sgna_change = (sgna/lead(sgna))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(sharefactor_change = (sharefactor/lead(sharefactor))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(sharesbas_change = (sharesbas/lead(sharesbas))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(shareswa_change = (shareswa/lead(shareswa))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(shareswadil_change = (shareswadil/lead(shareswadil))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(sps_change = (sps/lead(sps))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(tangibles_change = (tangibles/lead(tangibles))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(taxassets_change = (taxassets/lead(taxassets))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(taxexp_change = (taxexp/lead(taxexp))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(taxliabilities_change = (taxliabilities/lead(taxliabilities))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(tbvps_change = (tbvps/lead(tbvps))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(workingcapital_change = (workingcapital/lead(workingcapital))* 100.0)
stocks <- stocks %>% 
  group_by(ticker) %>% 
  mutate(stock_price_change = (ty_price/lead(ty_price))* 100.0)
# Save to csv
write.csv(stocks,"stocks-change-added.csv")

