install.packages("neuralnet", dependencies = TRUE)
install.packages("caret", dependencies = TRUE)
install.packages("forecast")
install.packages("NeuralNetTools")
library(neuralnet)
library(caret)
library(forecast)
library(tidyverse)
library(NeuralNetTools)

# Author: Kunnar Kukk
# Data Analytics Home Project

setwd("~/Desktop/andmeanalyytika/projekt")
stocks <- read_delim("cleaned-data-1-change-added.csv", ",", escape_double = FALSE, trim_ws = TRUE)

# Asendame NA väärtused 0-ga
stocks[is.na(stocks)] = 0
stocks <- stocks %>%
  mutate(stock_price_grows=ifelse(stock_price_change > 0, 1, 0))

# Training and validation set
set.seed(1)
training_index <- sample(row.names(stocks), 0.6*dim(stocks)[1])  
validation_index<- setdiff(row.names(stocks), stocks)  
training_data <- stocks[training_index, ]
validation_data <- stocks[validation_index, ]

# Normaliseerimine
normalized_values <- preProcess(training_data, method="range")
training_data_normalized <- predict(normalized_values, training_data)
validation_data_normalized <- predict(normalized_values, validation_data)

# Train neural network

###
## 2 bvps_change + capex_change
nn <- neuralnet(stock_price_grows ~ 
                + debt_change
                + assetsavg_change
                + assetsc_change
                + assetturnover_change
                + bvps_change
                + capex_change
                + cashneq_change
                + cashnequsd_change
                + cor_change
                + consolinc_change
                + currentratio_change
                + de_change
                + debt_change
                + debtc_change
                + debtnc_change
                + debtusd_change
                + deferredrev_change
                + depamor_change
                + deposits_change
                + divyield_change
                + dps_change
                + ebit_change
                + ebitda_change
                + ebitdamargin_change
                + ebitdausd_change
                + ebt_change
                + eps_change
                + epsdil_change
                + epsusd_change
                + equity_change
                + equityavg_change
                + equityusd_change
                + ev_change
                + evebit_change
                + evebitda_change
                + fcf_change
                + fcfps_change
                + fxusd_change
                + gp_change
                + grossmargin_change
                + intangibles_change
                + intexp_change
                + invcap_change
                + invcapavg_change
                + inventory_change
                + investments_change
                + investmentsc_change
                + investmentsnc_change
                + liabilities_change
                + liabilitiesnc_change
                + marketcap_change
                + ncf_change
                + ncfbus_change
                + ncfinv_change
                + ncfo_change
                + ncfx_change
                + netinc_change
                + netinccmn_change
                + netinccmnusd_change
                + netincdis_change
                + netincnci_change
                + netmargin_change
                + opex_change
                + opinc_change
                + payables_change
                + payoutratio_change
                + pb_change
                + pe_change
                + pe1_change
                + ppnenet_change
                + prefdivis_change
                + price_change
                + ps_change
                + ps1_change
                + receivables_change
                + retearn_change
                + revenue_change
                + revenueusd_change
                + rnd_change
                + roa_change
                + roe_change
                + roic_change
                + ros_change
                + sbcomp_change
                + sgna_change
                + sharefactor_change
                + sharesbas_change
                + shareswa_change
                + shareswadil_change
                + sps_change
                + tangibles_change
                + taxassets_change
                + taxexp_change
                + taxliabilities_change
                + tbvps_change
                + workingcapital_change, 
                data = training_data, linear.output = F, hidden = 3)
# display weights
nn$weights

plot(nn, rep="best")
training_prediction <- compute(nn, training_data_normalized)
nn$result.matrix

validation_prediction=compute(nn, validation_data_normalized)
table(unlist(validation_prediction))
hist(unlist(validation_prediction))

# Error: aktsia hind kasvab
accuracy(unlist(training_prediction), training_data_normalized$stock_price_grows)
garson(nn)
